package main

import (
	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pongg")
		// c.Redirect(http.StatusMovedPermanently, "http://35.153.236.165/")
	})
	return r
}

func main() {
	r := setupRouter()
	r.Run(":8080")
}
